import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import MyAwesomeReactComponent from './MyAwesomeReactComponent';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <TextField hintText="Message" />
          <RaisedButton label="Send" /> 
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;

// import React from 'react';
// import ReactDOM from 'react-dom';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// // import MyAwesomeReactComponent from './MyAwesomeReactComponent';
// import RaisedButton from 'material-ui/RaisedButton';
// import TextField from 'material-ui/TextField';


// const App = () => (
//   <TextField hintText="Message" />
//   <RaisedButton label="Send" /> 
//   // <MuiThemeProvider>
//   //   <MyAwesomeReactComponent />
//   // </MuiThemeProvider>
// );

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );

// // export default App;