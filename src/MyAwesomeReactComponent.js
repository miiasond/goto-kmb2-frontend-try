import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const MyAwesomeReactComponent = () => (
  <TextField hintText="Message" />
  <RaisedButton label="Send" />

);

export default MyAwesomeReactComponent;